const ReactDOM = require("react-dom");
const React = require("react");
const { Page } = require("./Components.jsx");
const createStore = require("./store");

/** @params object opts
 *  @params string opts.currentSession
 */
async function app(opts) {
  const route = await createStore(function(state) {
    ReactDOM.render(React.createElement(Page, state), opts.appElement);
  });
  document.addEventListener("click", function(e) {
    e.preventDefault();
    route(e.target.dataset.href);
    window.history.pushState(
      { route: e.target.dataset.href },
      "",
      e.target.dataset.href
    );
  });
  window.onpopstate = function(e) {
    route(e.state.route);
  };
  route(window.location.pathname);
}

// Initialize App
const appElement = document.getElementById("appContainer");
app({ appElement });
