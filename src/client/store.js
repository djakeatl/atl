async function getData() {
  const serverResponse = await fetch("/static/sessions.json");
  const body = await serverResponse.json();
  return body.Items;
}

async function createStore(listen) {
  const rawSessions = await getData();
  const uninitializedState = () => ({
    tracks: {
      allTracks: []
    },
    sessions: {},
    currentSessionId: rawSessions[0].Id,
    hompageSessionId: rawSessions[0].Id
  });
  const storage = rawSessions.reduce((state, session) => {
    const { sessions, tracks } = state;
    const trackTitle = session.Track.Title;
    const trackId = trackTitle.toLowerCase();
    if (!tracks[trackId]) {
      tracks[trackId] = Object.assign({}, session.Track);
      tracks[trackId].sessionIds = [session.Id];
    } else {
      tracks[trackId].sessionIds.push(session.Id);
    }
    if (!tracks.allTracks.includes(trackId))
      tracks.allTracks = tracks.allTracks.concat([trackId]);
    const sessionId = session.Id;
    if (!sessions[sessionId]) {
      session.trackId = trackId;
      session.descriptionParts = session.Description.split("\r\n");
      if (session.Speakers) {
        session.Speakers.forEach(speaker => {
          if (speaker.Biography) {
            speaker.biographyParts = speaker.Biography.split("\r\n");
          }
        });
      }
      sessions[sessionId] = session;
    }
    return state;
  }, uninitializedState());
  const store = {
    setCurrentSession(id) {
      if (!id) {
        id = storage.hompageSessionId;
        return;
      }
      if (!storage.sessions[id]) throw new Error(404);
      storage.currentSessionId = id;
    },
    setCurrentTrack(id) {
      if (!storage.tracks[id]) throw new Error(404);
      storage.currentSessionId = storage.tracks[id].sessionIds[0];
    }
  };
  function route(path = "/") {
    const parts = path
      .split("/")
      .filter(e => e !== "/")
      .filter(e => !!e);
    if (parts.length === 0) {
      store.setCurrentSession();
    }
    if (parts.length === 1) {
      store.setCurrentTrack(parts[0]);
    }
    if (parts.length === 2) {
      store.setCurrentSession(parts[1]);
    }
    listen.call(null, storage);
  }
  return route;
}

module.exports = createStore;
