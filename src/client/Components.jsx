// Page Components
const React = require("react");

exports.Page = ({ tracks, sessions, currentSessionId }) => {
  const currentTrack = tracks[sessions[currentSessionId].trackId];
  return (
    <div className="page-container">
      <header className="page-header header row">
        <img
          src="/static/Atlassian-horizontal-white-2x-rgb.png"
          alt="Atlassian"
          className="logo"
        />
      </header>
      <Hero />
      <div className="row">
        <TracksTabs>
          {tracks.allTracks.map(trackId => (
            <TrackTab
              key={trackId}
              id={trackId}
              title={tracks[trackId].Title}
              active={trackId === sessions[currentSessionId].trackId}
            />
          ))}
        </TracksTabs>
      </div>
      <div className="row">
        <Track {...currentTrack}>
          <ul className="sessions">
            {currentTrack.sessionIds.map(id => (
              <SessionTab
                key={id}
                {...sessions[id]}
                active={id === currentSessionId}
              />
            ))}
          </ul>
        </Track>
        <Session {...sessions[currentSessionId]} />
      </div>
    </div>
  );
};
// Data view components
const Track = ({ Title, children }) => (
  <div className="column">
    <h3 className="header">{Title}</h3>
    {children}
  </div>
);
const Hero = () => (
  <div className="hero row">
    <div className="header">
      <img
        src="/static/Atlassian-horizontal-blue-rgb.svg"
        alt="Atlassian Summit"
      />
      &nbsp;
      <span>Summit</span>
    </div>
    <div className="subtle">Video Archive</div>
  </div>
);
const TracksTabs = props => <nav className="tabs">{props.children}</nav>;
const TrackTab = props => (
  <a
    href={"/" + props.id}
    data-href={"/" + props.id}
    className={"tab" + ((props.active && " active") || "")}
  >
    {props.title}
  </a>
);
const SessionTab = ({ Speakers = [], active, ...session }) => (
  <li key={session.Id} className={"session" + (active ? " active" : "")}>
    <div className="header">
      <a
        href={`/${session.trackId}/` + session.Id}
        data-href={`/${session.trackId}/` + session.Id}
      >
        <strong>{session.Title}</strong>
      </a>
    </div>
    <div className="subtle">
      {Speakers.map(speaker => (
        <Speaker key={speaker.AttendeeID} {...speaker} />
      ))}
    </div>
  </li>
);
const Session = ({ Speakers = [], ...session }) => (
  <main className="main column">
    <h1 className="header">{session.Title}</h1>
    <strong>
      {Speakers.map(speaker => (
        <Speaker key={speaker.AttendeeID} {...speaker} />
      ))}
    </strong>
    {session.descriptionParts.map((part, i) => (
      <p key={i}>{part}</p>
    ))}
    <p dangerouslySetInnerHTML={session.descriptionHTML} />
    <p>
      <a href="#">
        <strong>See the the Q&amp;A from this talk and others here.</strong>
      </a>
    </p>

    {<SpeakersDetail speakers={Speakers} />}
  </main>
);
const Speaker = ({ FirstName, LastName, Company }) => (
  <p className="speaker">
    {FirstName} {LastName}, {Company}
  </p>
);
const SpeakersDetail = ({ speakers }) => {
  if (!speakers.length) return false;
  return (
    <section>
      <h2 className="header">
        About the speaker
        {speakers.length > 1 ? "s" : ""}
      </h2>
      {speakers.map(speaker => (
        <SpeakerDetail key={speaker.AttendeeID} {...speaker} />
      ))}
    </section>
  );
};
const SpeakerDetail = ({ biographyParts = [], ...speaker }) => (
  <div>
    <strong>
      <Speaker {...speaker} />
    </strong>
    {biographyParts.map((part, i) => (
      <p key={i}>{part}</p>
    ))}
  </div>
);
