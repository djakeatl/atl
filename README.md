### Atlassian Web Developer Code Exercise

To launch the app, clone this repo, then:

```
npm install
npm start
```

Once you see the text "Listening on port 3000", you can access the site at http://localhost:3000.
