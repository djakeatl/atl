const express = require("express");
const fs = require("fs");
const app = express();
const pageHTML = fs.readFileSync(__dirname + "/index.html");
app.use(
  "/static",
  express.static(__dirname + "/../static", {
    extensions: ["css", "ico", "svg", "png", "json"],
    fallthrough: false
  })
);
app.use(
  "/",
  express.static(__dirname + "/../public", { extensions: ["js", "json"] })
);
app.get("/:trackId?/:sessionId?", function(req, res) {
  res.set("content-type", "text/html");
  res.status(200).send(pageHTML);
});
const listener = app.listen(3000, function(err, server) {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`Listening on port ${listener.address().port}`);
});
